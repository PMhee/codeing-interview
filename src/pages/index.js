import React from "react";
import axios from "axios";
import Head from "next/head";
import { Card, Button, Collapse, Form, Spin, Modal } from "components";
import services from "services";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      offices: [],
      working: {},
      loading: false,
      formVisible: false,
      warningVisible: false
    };
  }
  _onConfirmDelete = () => {
    services.office.del(this.state.record.id).then(v => {
      this.setState({
        offices: (this.state.offices || []).filter(
          e => e.id !== this.state.record.id
        ),
        warningVisible: false
      });
    });
    services.working.del(this.state.record.workingSettingId);
  };
  _onClickDel = (e, v) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      warningVisible: true,
      record: v
    });
  };
  _onClickCreate = () => {
    this.setState({
      formVisible: true
    });
  };
  _onClickCollapse = v => {
    if (v.working === undefined) {
      services.working.get(v.workingSettingId).then(e => {
        if (e.data.length > 0) {
          this.setState({
            offices: (this.state.offices || []).map(f =>
              f.workingSettingId === e.data[0].id
                ? {
                    ...f,
                    working: {
                      ...e.data[0],
                      workingDays: (e.data[0].workingDays || []).map(g => ({
                        ...g,
                        id: e.data[0].id,
                        officeId: v.id
                      }))
                    }
                  }
                : f
            )
          });
        }
      });
    }
  };
  _onSubmit = v => {
    v.preventDefault();
    v.target.reset();
  };
  _onClickSave = record => {
    const {
      workingTimeStartAt,
      workingTimeEndAt,
      workingDays
    } = record.working;
    this.setState({
      loading: true
    });

    if (record.id) {
      services.office
        .put(record.id, {
          name: record.name,
          workingSettingId: record.workingSettingId
        })
        .then(v => {
          this.setState({
            offices: (this.state.offices || []).map(e =>
              e.id === v.data.id ? { ...e, name: v.data.name } : e
            ),
            loading: false
          });
        });
      services.working.put(record.workingSettingId, {
        workingTimeStartAt,
        workingTimeEndAt,
        workingDays
      });
    } else {
      services.working
        .post({ workingTimeStartAt, workingTimeEndAt, workingDays })
        .then(res => {
          services.office
            .post({
              name: record.name,
              workingSettingId: res.data.id
            })
            .then(office => {
              this.setState({
                offices: [...this.state.offices, { ...office.data }],
                loading: false,
                formVisible: false
              });
            });
        });
    }
  };
  componentDidMount() {
    services.office.get().then(v => {
      this.setState({
        offices: v.data
      });
    });
  }
  render() {
    return (
      <div style={{ padding: "20px" }}>
        <div>
          <Modal
            visible={this.state.warningVisible}
            header={
              <div>
                Warning
                <a
                  onClick={() =>
                    this.setState({
                      warningVisible: false
                    })
                  }
                  style={{
                    float: "right"
                  }}
                >
                  X
                </a>
              </div>
            }
          >
            <div>Are you sure you wanna delete the office?</div>
            <div style={{ color: "red" }}>
              This is an unrecoverable behavior. Once you delete it, all the
              related data will be gone.
            </div>
            <div
              style={{
                float: "right"
              }}
            >
              <Button
                style={{ marginRight: "12px" }}
                onClick={() =>
                  this.setState({
                    warningVisible: false
                  })
                }
              >
                Cancel
              </Button>
              <Button onClick={() => this._onConfirmDelete()} type="primary">
                Delete
              </Button>
            </div>
          </Modal>
          <Modal
            header={
              <div>
                Create New Setting
                <a
                  onClick={() =>
                    this.setState({
                      formVisible: false
                    })
                  }
                  style={{
                    display: "block",
                    position: "absolute",
                    right: 0,
                    top: 24
                  }}
                >
                  X
                </a>
              </div>
            }
            visible={this.state.formVisible}
          >
            <div>
              <form onSubmit={this._onSubmit}>
                <Form
                  record={{
                    working: {
                      workingDays: [
                        {
                          isEnabled: true,
                          dayType: "MON"
                        },
                        {
                          isEnabled: true,
                          dayType: "TUE"
                        },
                        {
                          isEnabled: true,
                          dayType: "WED"
                        },
                        {
                          isEnabled: true,
                          dayType: "THU"
                        },
                        {
                          isEnabled: true,
                          dayType: "FRI"
                        },
                        {
                          dayType: "SAT",
                          isEnabled: false
                        },
                        {
                          isEnabled: false,
                          dayType: "SUN"
                        }
                      ]
                    }
                  }}
                  onClickSave={this._onClickSave}
                  loading={this.state.loading}
                />
              </form>
            </div>
          </Modal>
          <Card
            header={
              <div>
                Setting - Working Days
                <Button
                  style={{ position: "absolute", right: 24 }}
                  type="primary"
                  onClick={() => this._onClickCreate()}
                >
                  Create New Setting
                </Button>
              </div>
            }
          >
            {this.state.offices.map(v => (
              <Collapse
                key={v.id}
                onClick={() => this._onClickCollapse(v)}
                header={
                  <div>
                    {v.name}
                    <a
                      onClick={e => this._onClickDel(e, v)}
                      style={{
                        position: "absolute",
                        right: 8,
                        display: "block",
                        top: 12
                      }}
                    >
                      🗑️
                    </a>
                  </div>
                }
              >
                <Form
                  record={v}
                  onClickSave={this._onClickSave}
                  loading={this.state.loading}
                />
              </Collapse>
            ))}
          </Card>
        </div>
      </div>
    );
  }
}

export default App;
