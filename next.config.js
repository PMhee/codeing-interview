const withSASS = require("@zeit/next-sass");
module.exports = withSASS({
  module: {
    loaders: [
      // the file-loader will copy the file and fix the appropriate url
      {
        test: /\.(ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "fonts/",
          publicPath: "../fonts/"
        }
      }
    ]
  }
  /* config options here */
});
