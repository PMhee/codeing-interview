import axios from "axios";
const office = {
  get: async () => {
    return await axios.get("http://localhost:3004/offices");
  },
  post: async data => {
    return await axios.post("http://localhost:3004/offices", data);
  },
  put: async (id, data) => {
    return await axios.put(`http://localhost:3004/offices/${id}`, data);
  },
  del: async id => {
    return await axios.delete(`http://localhost:3004/offices/${id}`);
  }
};
export default office;
