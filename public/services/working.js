import axios from "axios";
const working = {
  get: async id => {
    return await axios.get("http://localhost:3004/workingSettings", {
      params: { id: id }
    });
  },
  post: async data => {
    return await axios.post(`http://localhost:3004/workingSettings`, data);
  },
  put: async (id, data) => {
    return await axios.put(`http://localhost:3004/workingSettings/${id}`, data);
  },
  del: async id => {
    return await axios.delete(`http://localhost:3004/workingSettings/${id}`);
  }
};
export default working;
