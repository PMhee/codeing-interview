import React from "react";
class Modal extends React.Component {
  render() {
    return (
      <div
        className="modal"
        style={{ display: this.props.visible ? "block" : "none" }}
      >
        <div className="modal-content">
          {this.props.header && (
            <div className="modal-header">{this.props.header}</div>
          )}
          {this.props.children}
        </div>
      </div>
    );
  }
}
export default Modal;
