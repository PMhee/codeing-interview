import React from "react";
class Button extends React.Component {
  render() {
    return (
      <button
        type="submit"
        style={this.props.style}
        className={`${this.props.type && "primary"}`}
        onClick={this.props.onClick}
      >
        {this.props.children}
      </button>
    );
  }
}
export default Button;
