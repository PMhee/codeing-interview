import React from "react";
class Collapse extends React.Component {
  render() {
    return (
      <details onClick={this.props.onClick} style={{ marginBottom: "8px" }}>
        {this.props.header && (
          <summary style={{ position: "relative" }} className="collapse-header">
            {this.props.header}
          </summary>
        )}
        <div className="collapse-body">{this.props.children}</div>
      </details>
    );
  }
}
export default Collapse;
