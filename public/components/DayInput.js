import React from "react";
import { Input } from "components";
class DayInput extends React.Component {
  render() {
    return (
      <div style={{ marginBottom: "50px" }}>
        <div style={{ marginBottom: "4px" }}>
          <label>{this.props.label}</label>
        </div>
        {(this.props.days || []).map(v => (
          <div
            key={v.dayType}
            style={{
              display: "inline-block",
              padding: "8px",
              textAlign: "center",
              marginRight: "8px",
              backgroundColor: "#F8F8FA",
              width: "80px",
              marginBottom: "8px"
            }}
          >
            <div>{v.dayType}</div>
            <div style={{}}>
              <Input
                type="checkbox"
                defaultChecked={v.isEnabled}
                onChange={e => this.props.onDisableChange(e, v)}
              />
              <Input
                value={v.startAt}
                disabled={!v.isEnabled}
                type="time"
                onChange={e => this.props.onChangeStartTimeInDay(e,v)}
              />
              <Input
                value={v.endAt}
                disabled={!v.isEnabled}
                type="time"
                onChange={e => this.props.onChangeEndTimeInDay(e,v)}
              />
            </div>
          </div>
        ))}
      </div>
    );
  }
}
export default DayInput;
