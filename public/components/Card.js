import React from "react";
class Card extends React.Component {
  render() {
    return (
      <div className="card">
        {this.props.header && (
          <div className="card-header">{this.props.header}</div>
        )}
        <div className="card-body">{this.props.children}</div>
      </div>
    );
  }
}
export default Card;
