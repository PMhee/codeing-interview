import React from "react";
import { Input, DayInput, Button, Spin } from "components";
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  UNSAFE_componentWillReceiveProps(next) {
    if (
      next.record !== undefined &&
      next.record.working !== undefined &&
      this.state.record === undefined
    ) {
      this.setState({
        record: next.record
      });
    }
  }
  _onChangeName = ev => {
    this.setState({
      record: { ...this.state.record, name: ev.target.value }
    });
  };
  _onChangeStartTime = ev => {
    this.setState({
      record: {
        ...this.state.record,
        working: {
          ...this.state.record.working,
          workingTimeStartAt: ev.target.value,
          workingDays: (this.state.record.working.workingDays || []).map(v => ({
            ...v,
            startAt: ev.target.value
          }))
        }
      }
    });
  };
  _onChangeEndTime = ev => {
    this.setState({
      record: {
        ...this.state.record,
        working: {
          ...this.state.record.working,
          workingTimeEndAt: ev.target.value,
          workingDays: (this.state.record.working.workingDays || []).map(v => ({
            ...v,
            endAt: ev.target.value
          }))
        }
      }
    });
  };
  _onDisableChange = (ev, v) => {
    this.setState({
      record: {
        ...this.state.record,
        working: {
          ...this.state.record.working,
          workingDays: (this.state.record.working.workingDays || []).map(e =>
            e.dayType === v.dayType ? { ...e, isEnabled: ev.target.checked } : e
          )
        }
      }
    });
  };
  _onChangeStartTimeInDay = (ev, v) => {
    this.setState({
      record: {
        ...this.state.record,
        working: {
          ...this.state.record.working,
          workingDays: (this.state.record.working.workingDays || []).map(e =>
            e.dayType === v.dayType ? { ...e, startAt: ev.target.value } : e
          )
        }
      }
    });
  };
  _onChangeEndTimeInDay = (ev, v) => {
    this.setState({
      record: {
        ...this.state.record,
        working: {
          ...this.state.record.working,
          workingDays: (this.state.record.working.workingDays || []).map(e =>
            e.dayType === v.dayType ? { ...e, endAt: ev.target.value } : e
          )
        }
      }
    });
  };
  render() {
    return this.state.record ? (
      <div key={this.state.record.id}>
        {this.props.loading && <Spin />}
        <div style={{ position: "relative" }}>
          <Input
            ref={`office_name_${this.state.record.id}`}
            type="text"
            label="Office name"
            id={`office_name_${this.state.record.id}`}
            value={this.state.record.name}
            onChange={this._onChangeName}
          />
          {this.state.record.working ? (
            <div>
              <Input
                ref={`start_time_${this.state.record.working &&
                  this.state.record.working.id}`}
                type="time"
                label="Working Start Time (mm:ss, ex: 09:00)"
                id={`start_time_${this.state.record.working &&
                  this.state.record.working.id}`}
                value={
                  this.state.record.working &&
                  this.state.record.working.workingTimeStartAt
                }
                onChange={e => this._onChangeStartTime(e)}
              />
              <Input
                ref={`end_time_${this.state.record.working &&
                  this.state.record.working.id}`}
                type="time"
                label="Working Ending Time (mm:ss, ex: 09:00)"
                id={`end_time_${this.state.record.working.id}`}
                value={
                  this.state.record.working &&
                  this.state.record.working.workingTimeEndAt
                }
                onChange={e => this._onChangeEndTime(e)}
              />
              <DayInput
                onChangeStartTimeInDay={this._onChangeStartTimeInDay}
                onChangeEndTimeInDay={this._onChangeEndTimeInDay}
                onDisableChange={(e, v) => this._onDisableChange(e, v)}
                days={
                  (this.state.record.working &&
                    this.state.record.working.workingDays) ||
                  []
                }
              />
            </div>
          ) : null}

          <Button
            type="primary"
            style={{ position: "absolute", right: 0, bottom: -40 }}
            onClick={() => this.props.onClickSave(this.state.record)}
          >
            Save
          </Button>
        </div>
      </div>
    ) : (
      <Spin />
    );
  }
}
export default Form;
