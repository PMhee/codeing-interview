import React from "react";
class Input extends React.Component {
  render() {
    return (
      <div style={{ marginBottom: "12px" }}>
        <div style={{ marginBottom: "4px" }}>
          <label>{this.props.label}</label>
        </div>
        {this.props.value ? (
          <input
            onChange={this.props.onChange}
            disabled={this.props.disabled || false}
            style={{ lineHeight: "20px", padding: "4px" }}
            type={this.props.type}
            name={this.props.id}
            defaultChecked={this.props.defaultChecked}
            value={this.props.value}
          />
        ) : (
          <input
            onChange={this.props.onChange}
            disabled={this.props.disabled || false}
            style={{ lineHeight: "20px", padding: "4px" }}
            type={this.props.type}
            name={this.props.id}
            defaultValue={this.props.defaultValue}
            defaultChecked={this.props.defaultChecked}
          />
        )}
      </div>
    );
  }
}
export default Input;
