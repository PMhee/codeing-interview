import Card from "./Card";
import Button from "./Button";
import Collapse from "./Collapse";
import Input from "./Input";
import DayInput from "./DayInput";
import Form from "./Form";
import Spin from "./Spin";
import Modal from './Modal'
export { Card, Button, Collapse, Input, DayInput, Form, Spin,Modal };
