### ⭐️ About the Assignment

Hi there! :)
This assignment is actually the one real page from our product.
You are going to build a small app to handle the working time of employees for the offices.

<b>Please read the following requirements carefully, thanks!</b> 🙏🙏🙏

### ✔️ Requirements

###### - General part

- [ ] <b>No UI kit (Bootstrap, Ant Design, Material UI...etc ❌)</b>.
      But css-in-js libs such like styled-components, emotion...etc are welcome!

  > So we could see your CSS/LESS/SCSS skills and how you arrange it

- [ ] Write <b>unit tests</b> for the code

  > So we could see what you think about tests

- [ ] Consider the best approach of handling app's state and introduce it in the <b>document</b> like why you choose it. 🙇
  > So we could understand your idea of structuring the code, such like handling reused components, state... etc

###### - UI part

- [ ] It renders a list of offices from APIs
      ![](/images/index.png)
- [ ] After click the list, it will open and show the working setting of the office
      ![](/images/after-clicking-item.png)
- [ ] When changing the "Working Start Time" and "Working End Time", the related fields on the cards will be changed, so that users don't need to change it one by one on the cards.
      ![](/images/when-changing-time.png)
- [ ] But if the user directly change the card's fields, it will only affect the card.
      ![](/images/when-change-range-card.png)
- [ ] When clicking "Create New Setting" button, shows a modal with the form to create a new office and a new setting for the office. Clicking "X" will close the modal.
      ![](/images/after-clicking-create-button.png)
- [ ] When clicking delete icon will show a modal to ask the user to confirm it.
      ![](/images/after-clicking-delete-icon.png)

- [ ] The app can create, update, delete offices and settings correctly.

### APIs

This mock data server was created by [json-server](https://github.com/typicode/json-server) and designed for the assignment. You can check the `db.json` to see the details of mock database.

1. `yarn install` or `npm install` to install node modules
2. `yarn start` or `npm start` to start the API server on `http://localhost:3004`

Press `s + enter` at any time to create a snapshot of the database, so that you could easily to roll back the version of the database. It's really useful when you are doing some tests on apis.

The followings are the APIs you might need.

###### Office

- Get offices

  > GET http://localhost:3004/offices

- Create office

  > POST http://localhost:3004/offices
  > Data format: { name, workingSettingId }

- Update office

  > PUT http://localhost:3004/offices/{officeId}
  > Data format: { name, workingSettingId }

- Delete office

  > DELETE http://localhost:3004/offices/{officeId}

- Data format details
  - name: The name of office
  - workingSettingId: The id of the setting

###### Working Day Settings

- Get specific working day setting

  > GET http://localhost:3004/workingSettings?id={id}

- Create setting

  > POST http://localhost:3004/workingSettings
  > Data format: { workingTimeStartAt, workingTimeEndAt, workingDays }

- Update setting

  > PUT http://localhost:3004/workingSettings/{settingId}
  > Data format: { workingTimeStartAt, workingTimeEndAt, workingDays }

- Delete setting

  > DELETE http://localhost:3004/workingSettings/{settingId}

- Data format details

  - workingDays

    > example: `[{ isEnabled: true, startAt: "09:00", endAt: "18:00", dayType: "MON" }]`

    <b>dayType</b>: Name of the weekday, could be MON, TUE, WED, THU, FRI, SAT, SUN.

    <b>isEnabled</b>: To represent employees should work on this day or not.

    <b>startAt</b>: Time of starting work

    <b>endAt</b>: Time of off work

    <br>

  - workingTimeStartAt: Time of starting work

    > example: "10:00"

  - workingTimeEndAt: Time of off work

    > example: "19:00"
